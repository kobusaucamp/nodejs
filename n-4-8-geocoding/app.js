const request = require('request');
const yargs = require('yargs');

const argv = yargs
  .options({
    a: {
      demand: true,
      alias: 'address',
      describe: 'Address to fetch weather for',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

var encodedAddress = encodeURIComponent(argv.address);

var KEY = '5MBgnEFzSzBEa5pHFsOuwAecdhMxGPTI';
request({
  url: `http://www.mapquestapi.com/geocoding/v1/address?key=${KEY}&location=${encodedAddress}`,
  json: true
}, (error, response, body) => {
  // console.log(JSON.stringify(response, undefined, 2));
  if (error) {
    console.log('Unable to connect to MapQuest servers.');
  } else {
    console.log(`Address: ${body.results[0].locations[0].street}, ${body.results[0].locations[0].adminArea3} ${body.results[0].locations[0].postalCode}, ${body.results[0].locations[0].adminArea1}`);
    console.log(`Latitude: ${body.results[0].locations[0].latLng.lat}`);
    console.log(`Longitude: ${body.results[0].locations[0].latLng.lng}`);
  }
});
