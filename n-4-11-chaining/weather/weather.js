const request = require('request');

WEATHER_KEY = '322bdda136377132daec7026882d6e6a';
var getWeather = (lat, lng, callback) => {
  request({
    url: `https://api.forecast.io/forecast/${WEATHER_KEY}/${lat},${lng}?units=auto`,
    json: true
  }, (error, response, body) => {
    if (error) {
      callback('Unable to connect to Forecast.io server.');
    } else if (response.statusCode === 400) {
      callback('Unable to fetch weather.');
    } else if (response.statusCode === 200) {
      callback(undefined, {
        temperature: body.currently.temperature,
        apparentTemperature: body.currently.apparentTemperature
      });
    }
  });
};

module.exports.getWeather = getWeather;
