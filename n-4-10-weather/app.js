// const yargs = require('yargs');
//
// const geocode = require('./geocode/geocode');
//
// const argv = yargs
//   .options({
//     a: {
//       demand: true,
//       alias: 'address',
//       describe: 'Address to fetch weather for',
//       string: true
//     }
//   })
//   .help()
//   .alias('help', 'h')
//   .argv;
//
// geocode.geocodeAddress(argv.address, (errorMessage, results) => {
//   if (errorMessage) {
//     console.log(errorMessage);
//   } else {
//     console.log(JSON.stringify(results, undefined, 2));
//   }
// });

const request = require('request');

WEATHER_KEY = '322bdda136377132daec7026882d6e6a';
// request({
//   url: `https://api.forecast.io/forecast/${WEATHER_KEY}/${geoinfo.latitude},${geoinfo.longitude}`,

request({
  url: `https://api.forecast.io/forecast/${WEATHER_KEY}/39.9396284,-75.18663959999999`,
  json: true
}, (error, response, body) => {
  if (!error && response.statusCode === 200) {
    console.log(body.currently.temperature);
  } else {
    console.log('Unable to fetch weather.');
  }
});
