console.log('Starting app.');

const fs = require('fs');
const os = require('os');

var user = os.userInfo();
var upTime = os.uptime() / 60;

fs.appendFileSync('greetings.txt', `Hello ${user.username}, uptime: ${upTime} minutes!`);
