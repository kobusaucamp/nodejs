module.exports.add = (a, b) => a + b;

module.exports.square = (x) => x * x;

module.exports.sub = (a, b) => a - b;