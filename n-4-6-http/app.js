const request = require('request');

var KEY = '5MBgnEFzSzBEa5pHFsOuwAecdhMxGPTI';

request({
  url: `http://www.mapquestapi.com/geocoding/v1/address?key=${KEY}&location=1301%20lombard%20street%20philadelphia`,
  json: true
}, (error, response, body) => {
  // console.log(JSON.stringify(error, undefined, 2));
  console.log(`Address: ${body.results[0].locations[0].street}, ${body.results[0].locations[0].adminArea4}, ${body.results[0].locations[0].adminArea3} ${body.results[0].locations[0].postalCode}, ${body.results[0].locations[0].adminArea1}`);
  console.log(`Latitude: ${body.results[0].locations[0].latLng.lat}`);
  console.log(`Longitude: ${body.results[0].locations[0].latLng.lng}`);
});
